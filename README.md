# SPA with WordPress REST API and Vue.js  - Creat and View a list of clients / users
<b><i>Single Page Application APP for understanding WP RESTful Endpoints with Vue.js</i></b>

This small APP is based on Wordpress REST API technology and Vue.js framework for frontend. Following instructions and 
checking all files can lead to better understanding of the WP REST API structure in general and a simplified way to build SPA's with WordPress. 

LIVE DEMO LINK:<br>
[https://wp-api.agonxheladini.com/](https://wp-api.agonxheladini.com/)

## Prerequisite
* WordPrese Core basic
* WordPress Custom Post Types 
* WordPress RESTFul API structure and Basic Auth plugin
* Intermediate Vue.js

## Requirements and Instalation

I Assume that you already have a fresh instalation of WordPress on your local machine or into a server. 

<b>1. </b>A child theme has been created on top of Beans theme. Therefore as a first step you must install Beans theme within your theme directory. Beans is a very good starter theme. 

Point to your theme directory and clone Beans theme: 

```
git clone https://github.com/Getbeans/Beans.git
```

Rename Beans theme to tm-beans -> important

```
mv Beans tm-beans
```



<b>2. </b> I have choosed Basic Authentication for this example and for that we need <b>JSON Basic Authentication</b> plugin.

Point to your plugin directory and clone the Basic Authenication plugin: 

```
https://github.com/WP-API/Basic-Auth.git
```
<i>Login to the dashboard and enable this plugin -> important </i>



<b>3. </b>Point to your theme directory again and clone this child theme / WP REST API (the repository itself).
```
https://gitlab.com/agonx/wordpress-spa-with-wp-restfulapi-and-vue.git
```

Rename it to wp-spa / important

```
mv wordpress-spa-with-wp-restfulapi-and-vue wp-spa
```



<b>4. </b> To speed up the proceess of coding and structuring the APP I have used a ready made Custom Post Typs (CPT) class by <b>jjgrainger</b> for creating the CPT 'Client'. The same class is imported with composer. 
The repo for this class can be found here: 
[https://github.com/jjgrainger/wp-custom-post-type-class](https://github.com/jjgrainger/wp-custom-post-type-class)

Point to the child theme <b>wp-spa</b> than <b>vendor</b> and delete jjgrainger directory. 
Go back to child theme and update composer to download a fresh copy oth the class: 

```
composor update --dev-master
```



<b>5. </b> With this app a 'client'/'user' can use a form and register to  our platform by using WP REST API. There is a need for authentication method, since it is an demo project i have used Basic Authentication method. For enterprise usage I strongly recommand OAuth2 as authentication method. 
For this reason in the child theme directory inside <b>include</b> directory search for a file called <i>ax_wp_spa_enqueue_assets.php</i>, on line 21 replace username and password with your WP user information (it must have adminstrator privileges). 

```
wp_localize_script( 'app_vue_js', 'axSettings', array( 'root' => esc_url_raw( rest_url() ), 'nonce' => wp_create_nonce( 'wp_rest' ), 'username'=>'admin', 'passwd' => 'admin' ) );
```



<b>6. </b>After you pass all steps update login to the dashboard and switch to the child theme called : ax wp RESTful API 

* Developed under Wordpress version: 4.9.4, it works with latest WP version 5.3.1
* Vue.js v2.5.13 